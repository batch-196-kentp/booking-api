const express = require("express");
const mongoose = require("mongoose");
/*
	Moongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database.
*/
const app = express();
const port = 4000;
//express.json() to be able to handle the request body and parse it into JS Object.
/*
	Mongoose Connection 

	moongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. First, is the connection string to connect our API to mongodb. Second, is an object used to add information between mongoose and mongodb.
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.5ykhik8.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});
//create notifications if the connection to the db is a success or failed.
let db = mongoose.connection;
//show notification of an internal server erorr from MongoDB
db.on('error', console.error.bind(console,"MongoDB Connection Error."));
//IF the connection is open and successful, we will output a message in the terminal/gitbash:
db.once('open',()=>console.log("Connected to MongoDB."))

app.use(express.json());

//import our routes and use it as middleware.
//which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);

//user our routes and group them together under '/courses'
//our endpoints are now prefaced with /courses
app.use('/courses',courseRoutes);
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

app.listen(port,() => console.log(`Express API running at port 4000`));