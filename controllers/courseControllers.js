// Import the course model so we can manipulate it and add a new course document.
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res) =>{

    //Use the Course model to connect to our collection and retrieve our courses.
    //TO be able to query into our collections we use the model connected to that collection.
    //in mongoDB: db.course.find)({})
    //Model.find() - returns a collection of documents that matches our criteria similar to mongodb's find()
    Course.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.addCourse = (req,res) =>{

    // console.log(req.body);

    // Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model, and add methods for document creation.

    let newCourse = new Course({

        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })
    console.log(newCourse);
    //newCourse is now an object which follows the copurseSchema and with additional methods from our Course constructor.
    //.save() method is added into our newcCourse object. This will allow us to save the content of newCourse into our collection.
    //db.courses.insertOne() --if from db
    newCourse.save()

    //.then() allows us to process the result of a previous function/method
    .then(result => res.send(result))
    .catch(error => res.send(error))
}
module.exports.getActiveCourses = (req,res) => {
    Course.find({isActive: true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}
module.exports.getSingleCourse = (req,res) => {
  
    //console.log(req.params);
    //req.params is an object that contains the value captured via route params
    //the field name of the req.params indicate the name of the route paramsd

    console.log(req.params.courseId);

    Course.findById(req.params.courseId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.updateCourse = (req,res)=> {

    //check if we can get the id
    console.log(req.params.courseId);

    //check the update that is inputted
    console.log(req.body)

    //findByIdAndUpdate - used to update documents and has 3 arguments
    //findByIdAndUpdate(<id>,{updates},{new:true})

    //WE can create a new object to filter update details
    let update = {

        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.archiveCourse = (req,res) => {
    // console.log(req.params.courseId)
    // console.log(req.body)
    let update = {
        isActive:false

    }

    Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}