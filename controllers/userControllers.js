//userControllers.js
/*
	Naming convention for controllers is that is should be named after the model/documents it is concerned with.



//Controller are function which contain the actual business logic of our api and is triggered by a route.

//MV - models, view and controller
*/

//TO create a controller, we first add it into our module.exports.
//SO that we can import the controllers from our module.

//import the User model in the controllers instead because this is wherewe are now going to use it.
const User = require("../models/User");
//import bcrypt
//bcrypt is a package which allows us to has our password to add a layer of security for our users' details
const bcrypt = require("bcrypt");

//import auth.js module to use createAccessToken and its subsequent methods
const auth = require("../auth");


//import Course
const Course = require("../models/Course")



module.exports.registerUser = (req,res)=>{

	// console.log(req.body)//check the input passed via the client

	//using bcrypt, we're going to hide the suer's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash.
	//bcrypt.hashsync(<string>,<saltRounds>)
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.	mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getUserDetails = (req,res)=>{
	//console.log(req.user)//contains the details of the logged in user

	//This will allow us to ensure that the LOGGED IN USER or the USER THAT PASSED THE TOKEN will be able to get HIS OWN details and ONLY his own.
	User.findById(req.user.id)
	.then(result => res.send(result))
    .catch(error => res.send(error))
}

//checks if email exists or not
module.exports.checkEmail = (req,res) => {
    User.findOne({email:req.body.email})
    .then(result => {
    	//findOne will return null if no match is found
    	//Send false if email does not exist
    	//Send true if email exists
    	if(result === null){
    		return res.send(false)
    	} else {
    		return res.send(true)
    	}
    })
    .catch(error => res.send(error))
}

module.exports.loginUser = (req,res)=>{

	console.log(req.body);
	/*
		Steps for logging in our user:
		1. find the user by its email
		2. IF we find the user, check his password if the password input and the hashed password in our db matches.
		3. IF we can't find a user, we will send a message to the client.
		4. IF upon checking the found user's password is the same tothe input password, we will generate a "key" for the suer to have authorization to access certain features in our app.
	*/
	//mongodb: db.users.findOne({email:req.body.email})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if it is not able to match any document
		if(foundUser === null) {
			return res.send({message: "No User Found."})
			//client will receive this object with our message if no user is found
		} else {
			//console.log(foundUser)
			//If we find a user, foundUser will contain the document that matched the input email.
			//Check if the input password from req.body matches the hashed password in our foundUser document.
			/*
				Syntax of compareSync:
				bcrypt.compareSync(<inputString>,<hashedString>)

				if the inputString and hashedString matches, the compareSync method will return true.
				else, it will return false
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			//console.log(isPasswordCorrect);
			//IF the password is correctm we will create a "key", a token for our user, else, we will send a message:
			if(isPasswordCorrect){
				
				// console.log("We will create a token for the user if the password is correct.");
				/*
					TO be able to create a "key" or token that allows/authorizes our logg in user around our application, we have to create our own module called auth.js.

					This module will create an encoded string which contains our users' details.

					This encoded string is what we call a JSONWebToken or JWT.

					This JWT can only be properly unwrapped or decode with out own secret word/string.
				*/

				//auth.createAccessToken receive our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to our client.
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Incorrect Password"});
			}

		}
	})

}

module.exports.enroll = async (req,res) => {

	//check the id of the user who will enroll
	console.log(req.user.id);

	//check the id of the course we want to enroll
	console.log(req.body.courseId);

	//Validate the user if they are an admin or not.
	//If the user is an admin, send a message to client and end the response
	//Else, we will continue
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}
	/*
		Enrollment will come in 2 steps

		First, find the user who is enrolling and update his enrollments and subdocument array. We will push the courseId in the enrollments array.
		Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

		Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line.
		
		async and await ---async keyword is added to a function to makte or function asynchronous. Which means that instead of JS regular behavior of running each code line by line, we will be able to wait for teh result of a funtion
		
		To be able to wait for the result of a function, we use the await keyword. the await keyword allows us to wait for the function to finish and get a result before proceeding.

	*/

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		//check if you found the user's document
		// console.log(user)

		//Add the courseId in an object and push that object into the user's enrollments.
		//Because we have to follow the schema of the enrollments subdocument array:
		let newEnrollment = {
			courseId: req.body.courseId
		}


		//access the enrollments array from our user and push the new enrollment subdocument into the enrollments array
		user.enrollments.push( newEnrollment);


		//We must save the user document and return  the value of saving our document.
		//We will return true if we push the subdocument successfully
		//catch and return error message otherwise
		return user.save().then(user => true).catch(err => err.message)

	})
	//If user was able to enroll properly, isUserUpdated contains true.
	//Else, isUserUpdated will contain an error message
	//console.log(isUserUpdated)

	//Add an if statement and stop the process IF isUserUpdated DOES not contain true
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	//Find the course where we will enroll or add the user as an enrollee and return true IF we were able to push the user into the enrolless array properly or send the error message instead.
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		// console.log(course);
		//contain the found course or the course we want to enroll in



		//create an object to pushed into the subdocument array, enrolles
		//we have to follow the schema of our subdocument array
		let enrollee = {
			userId: req.user.id
		}

		//push the enrolle into the enrolless subdocument array of the course:
		course.enrollees.push(enrollee);

		//save the course document
		//Return true IF we were able to save and add the user as enrollee properly
		//Return an err message if we catch an error.
		return course.save().then(course => true).catch(err => err.message);


	})

	//console.log(isCourseUpdated);

	//If isCourseUpdated does not contain true, send the error message to the client and stop the process.
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	//Ensure that we were able to both update the user and course document to add our enrollment and enrolle respectively and send a message to the client to end the enrollment process:
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling!"})
	}
}
